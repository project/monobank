<?php

namespace Drupal\monobank\Plugin\Basket\Payment;

use Drupal\basket\Plugins\Payment\BasketPaymentInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\monobank\Form\PaymentForm;
use Drupal\monobank\Controller\Pages;

/**
 * Class of BasketMonobank.
 *
 * @BasketPayment(
 *   id = "monobank",
 *   name = "Monobank",
 * )
 */
class BasketMonobank implements BasketPaymentInterface {

  use DependencySerializationTrait;

  /**
   * Drupal\basket\Basket definition.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Drupal\monobank\Monobank definition.
   *
   * @var \Drupal\monobank\Monobank
   */
  protected $monobank;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->basket = \Drupal::service('Basket');
    $this->monobank = \Drupal::service('Monobank');
  }

  /**
   * Alter for making adjustments when creating a payment item.
   */
  public function settingsFormAlter(&$form, $form_state) {
    $tid = $form['tid']['#value'];
    $form['settings'] = [
      '#type' => 'details',
      '#title' => t('Settings'),
      '#open' => TRUE,
      '#parents' => ['basket_payment_settings'],
      '#tree' => TRUE,
      'status' => [
        '#type' => 'select',
        '#title' => $this->monobank->t('Change order status after payment to'),
        '#options' => $this->basket->Term()->getOptions('fin_status'),
        '#empty_option' => $this->monobank->t('Do not change status'),
        '#default_value' => $this->basket->getSettings('payment_settings', $tid . '.status'),
      ],
    ];
    $form['#submit'][] = [$this, 'formSubmit'];
  }

  /**
   * Alter for making adjustments when creating a payment item (submit)
   */
  public function formSubmit($form, $form_state) {
    $tid = $form_state->getValue('tid');
    if (!empty($tid)) {
      $this->basket->setSettings(
        'payment_settings',
        $tid,
        $form_state->getValue('basket_payment_settings')
      );
    }
  }

  /**
   * Interpretation of the list of settings for pages with payment types.
   */
  public function getSettingsInfoList($tid) {
    $items = [];
    if (!empty($settings = $this->basket->getSettings('payment_settings', $tid))) {
      $value = $this->monobank->t('Do not change status');
      if (!empty($settings['status']) && !empty($term = $this->basket->term()->load($settings['status']))) {
        $value = $this->basket->translate()->trans($term->name);
      }
      $items[] = [
        '#type' => 'inline_template',
        '#template' => '<b>{{ label }}: </b> {{ value }}',
        '#context' => [
          'label' => $this->monobank->t('Change order status after payment to'),
          'value' => $value,
        ],
      ];
    }
    return $items;
  }

  /**
   * CreatePayment.
   *
   * Creation of payment
   * return [
   * 'payID' => 10,
   * 'redirectUrl' => \Drupal::url('payment.routing')
   * ];.
   */
  public function createPayment($entity, $order) {
    if (!empty($order->pay_price)) {

      $ccy = \Drupal::config('monobank.settings')->get('config.currency');
      $currency = $this->basket->currency()->load($order->pay_currency);
      if (!empty($currency)) {
        $payCcy = array_search($currency->iso, $this->monobank->getCurrency());
        if (!empty($payCcy)) {
          $ccy = $payCcy;
        }
      }

      $payment = $this->monobank->load([
        'nid' => $entity->id(),
        'create_new' => TRUE,
        'amount' => $order->pay_price,
        'currency' => $ccy
      ]);
      if (!empty($payment)) {
        return [
          'payID' => $payment->id,
          'redirectUrl' => NULL,
        ];
      }
    }
    return [
      'payID' => NULL,
      'redirectUrl' => NULL,
    ];
  }

  /**
   * LoadPayment.
   *
   * Downloading Payment Data
   * return [
   * 'payment' => object,
   * 'isPay' => TRUE/FALSE
   * ];.
   */
  public function loadPayment($id) {
    $payment = $this->monobank->load(['id' => $id]);
    if (!empty($payment)) {
      return [
        'payment' => $payment,
        'isPay' => $payment->status != 'new' ? TRUE : FALSE,
      ];
    }
  }

  /**
   * Alter page redirects for payment.
   */
  public function paymentFormAlter(&$form, $form_state, $payment) {
    (new PaymentForm)->basketPaymentFormAlter($form, $form_state, $payment);
  }

  /**
   * BasketPaymentPages.
   *
   * Alter processing pages of interaction between
   * the payment system and the site
   * $pageType = callback/result/cancel.
   */
  public function basketPaymentPages($pageType) {
    return (new Pages)->pages('status');
  }

  /**
   * Update order by settings.
   *
   * @param int $pid
   *   The product ID.
   * @param object $orderClass
   *   The order class instance.
   */
  public function updateOrderBySettings($pid, $orderClass) {
    $order = $orderClass->load();
    if (!empty($order) && !empty($settings = $this->basket->getSettings('payment_settings', $pid))) {
      if (!empty($settings['status'])) {
        $order->fin_status = $settings['status'];
        $orderClass->replaceOrder($order);
        $orderClass->save();
      }
    }
  }

}
