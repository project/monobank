<?php

namespace Drupal\monobank;

use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class of Monobank.
 */
class Monobank {

  const API_URL = 'https://api.monobank.ua/api/merchant/invoice/';

  /**
   * Drupal\mysql\Driver\Database\mysql\Connection definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs a new Monobank object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    $this->config = \Drupal::config('monobank.settings');
  }

  /**
   * Translates a string to the specified language.
   *
   * @param string $string
   *   The string to be translated.
   * @param array $args
   *   (optional) An array of arguments to replace placeholders in the string.
   */
  public function t(string $string, array $args = []) {
    return new TranslatableMarkup($string, $args, ['context' => 'monobank']);
  }

  /**
   * Loads a payment based on the given parameters.
   *
   * @param array $params
   *   An array of parameters to filter the payment.
   */
  public function load(array $params) {
    $query = $this->database->select('payments_monobank', 'l');
    $query->fields('l');
    if (!empty($params['id'])) {
      $query->condition('l.id', $params['id']);
    }
    if (!empty($params['nid'])) {
      $query->condition('l.nid', $params['nid']);
    }
    if (!empty($params['sid'])) {
      $query->condition('l.sid', $params['sid']);
    }
    if (empty($params['data'])) {
      $params['data'] = [];
    }
    $payment = $query->execute()->fetchObject();

    if (!empty($params['create_new'])) {
      $payment = FALSE;
    }

    if (empty($payment) && !empty($params['amount'])) {
      $payment = (object) [
        'nid' => !empty($params['nid']) ? $params['nid'] : NULL,
        'sid' => !empty($params['sid']) ? $params['sid'] : NULL,
        'uid' => \Drupal::currentUser()->id(),
        'created' => time(),
        'paytime' => NULL,
        'amount' => $params['amount'],
        'currency' => !empty($params['currency']) ? $params['currency'] : \Drupal::config('monobank.settings')->get('config.currency'),
        'status' => 'new',
        'data' => serialize($params['data']),
      ];
      $payment->id = $this->database->insert('payments_monobank')
        ->fields((array) $payment)
        ->execute();
    }
    return $payment;
  }

  /**
   * Updates a payment record in the 'payments_monobank' table.
   *
   * @param object $payment
   *   The payment object to be updated.
   */
  public function update($payment) {
    if (!empty($payment->id)) {
      $updateFields = (array) $payment;
      unset($updateFields['id']);
      $this->database->update('payments_monobank')
        ->fields($updateFields)
        ->condition('id', $payment->id)
        ->execute();
    }
  }

  /**
   * Retrieves the status of a payment.
   *
   * @param int $pid
   *   (optional) The payment ID. Defaults to 0.
   */
  public function getStatus($pid = 0) {
    $payment = $this->load(['id' => $pid]);
    if (!empty($payment)) {
      $raw = unserialize($payment->data);

      $config = $this->config->get('config');

      // Alter:
      $data = [];
      \Drupal::moduleHandler()->alter('monobank_payment_params', $data, $payment, $config);

      // Method give:
      $token = (string) @$config['key'];

      try {
        $client = \Drupal::httpClient();
        $response = $client->get($this::API_URL . 'status?invoiceId=' . $raw['insert']['invoiceId'], [
          'headers' => [
            'Content-Type' => 'application/json',
            'X-Token' => trim($token),
          ],
        ]);
        $resData = $response->getBody()->getContents();
        if (!empty($resData)) {
          $resData = @json_decode($resData, TRUE);
        }
      }
      catch (\Exception $e) {
        \Drupal::messenger()->addMessage($e->getMessage(), 'error');
      }

      if (!empty($resData) && !empty($resData['status'])) {
        $raw['status'] = $resData;
        $update = (object) [
          'id' => $payment->id,
          'status' => $resData['status'],
          'data' => serialize($raw),
        ];
        $this->update($update);

        if ($resData['status'] === 'success') {
          $basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;

          if (!empty($payment->nid) && !empty($basket)) {
            if (method_exists($basket, 'paymentFinish')) {
              $basket->paymentFinish($payment->nid);
            }
          }

          // Alter:
          \Drupal::moduleHandler()->alter('monobank_api', $payment, $_POST);

          // Trigger change payment status by order:
          if (!empty($payment->nid) && !empty($basket)) {
            $order = $basket->orders(NULL, $payment->nid)->load();
            if (!empty($order) && \Drupal::moduleHandler()->moduleExists('basket_noty')) {
              \Drupal::service('BasketNoty')->trigger('change_monobank_status', [
                'order' => $order,
              ]);
            }
          }
        }

        return $resData['status'];
      }

    }
  }

  /**
   * Get a list of currency.
   */
  public function getCurrency() {
    $list = [
      980 => 'UAH',
      978 => 'EUR',
      840 => 'USD'
    ];
    \Drupal::moduleHandler()->alter('monobank_currency_list', $list);
    return $list;
  }

}
