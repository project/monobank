<?php

namespace Drupal\monobank\Controller;

use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class of Pages.
 */
class Pages {

  /**
   * Monobank service.
   *
   * @var \Drupal\monobank\Monobank
   */
  protected $monobank;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->monobank = \Drupal::service('Monobank');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
    $this->config = \Drupal::config('monobank.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function title(array $_title_arguments = [], $_title = '') {
    return t($_title, $_title_arguments, ['context' => 'monobank']);
  }

  /**
   * Retrieves the pages based on the page type.
   *
   * @param string $page_type
   *   The type of the page.
   */
  public function pages($page_type) {
    $element = [];
    switch ($page_type) {
      case'pay':
        $is_404 = TRUE;
        $payment = $this->monobank->load([
          'id' => $this->query['pay_id'] ?? '-',
        ]);
        if (!empty($payment) && $payment->status == 'new') {
          $is_404 = FALSE;
          $element['form'] = \Drupal::formBuilder()->getForm('\Drupal\monobank\Form\PaymentForm', $payment);
        }
        if ($is_404) {
          throw new NotFoundHttpException();
        }
        break;

      case'status':
        $_POST = @json_decode(file_get_contents('php://input'), TRUE);

        if (empty($_POST['invoiceId'])) {
          throw new NotFoundHttpException();
        }

        $config = \Drupal::config('monobank.settings')->get('config');
        $payment = $this->monobank->load([
          'id' => $_GET['id'] ?? '-',
        ]);

        if (!empty($payment->currency) && (string) $payment->currency === (string) $_POST['ccy'] && (string) ($payment->amount * 100) === (string) $_POST['amount']) {
          $prePay = @unserialize($payment->data);

          if ($prePay['insert']['invoiceId'] == (string) @$_POST['invoiceId']) {
            $status = $_POST['status'] ?? '';
            $prePay['status'] = $_POST;
            $update = [
              'id' => $payment->id,
              'data' => serialize($prePay),
            ];
            if ($status == 'success') {
              $update['paytime'] = time();
            }
            if (!empty($status)) {
              $update['status'] = $status;
            }

            $this->monobank->update((object) $update);

            if ($status == 'success') {

              if (!empty($payment->nid) && !empty($this->basket)) {
                if (method_exists($this->basket, 'paymentFinish')) {
                  $this->basket->paymentFinish($payment->nid);
                }
              }
              // Alter:
              \Drupal::moduleHandler()->alter('monobank_api', $payment, $_POST);

              // Trigger change payment status by order:
              if (!empty($payment->nid) && !empty($this->basket)) {
                $order = $this->basket->Orders(NULL, $payment->nid)->load();
                if (!empty($order) && \Drupal::moduleHandler()->moduleExists('basket_noty')) {
                  \Drupal::service('BasketNoty')->trigger('change_monobank_status', [
                    'order' => $order,
                  ]);
                }
              }
              exit('OK');
            }
          }
        }
        exit('Sign wrong');

      case 'payment_result':
        $element['#title'] = $this->monobank->t('Payment result');
        $pid = $_GET['id'] ?? 0;
        $payON = FALSE;
        if (!empty($pid)) {
          $payment = $this->monobank->load(['id' => $pid]);
          if (!empty($payment->status) && $payment->status == 'success') {
            $payON = TRUE;
          }
          if (!$payON) {
            $getStatus = $this->monobank->getStatus($pid);
            if (!empty($getStatus) && $getStatus == 'success') {
              $payON = TRUE;
            }
          }
          if ($payON) {
            $config = \Drupal::config('monobank.settings')->get('config');
            $success = !empty($config['success']) ? $config['success'] : '';
            $element += [
              '#theme' => 'monobank_success_page',
              '#info' => [
                'text' => [
                  '#type' => 'processed_text',
                  '#text' => !empty($success['value']) ? $success['value'] : '',
                  '#format' => !empty($success['format']) ? $success['format'] : NULL,
                ],
                'type' => 'success',
              ],
              '#attached' => [
                'library' => ['monobank/css'],
              ],
            ];
          }
          else {
            $element += [
              '#theme' => 'monobank_success_page',
              '#info' => [
                'text' => [
                  'text' => [
                    '#type' => 'processed_text',
                    '#text' => '<p class="text-align-center"><strong>' . $this->monobank->t('No payment data.') . '</strong></p>',
                    '#format' => 'full_html',
                  ],
                  'link' => [
                    '#type' => 'link',
                    '#title' => $this->monobank->t('Repeat'),
                    '#url' => new Url('monobank.pages', [
                      'page_type' => 'pay',
                    ], [
                      'attributes' => [
                        'class' => ['form-submit'],
                      ],
                      'query' => [
                        'pay_id' => $pid,
                      ],
                    ]),
                  ],
                ],
                'type' => 'error',
              ],
              '#attached' => [
                'library' => ['monobank/css'],
              ],
            ];
          }
        }
        break;
    }
    return $element;
  }

}
