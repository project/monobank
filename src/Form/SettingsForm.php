<?php

namespace Drupal\monobank\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Class of SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * Monobank service.
   *
   * @var \Drupal\monobank\Monobank
   */
  protected $monobank;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->monobank = \Drupal::service('Monobank');
    $this->ajax = [
      'wrapper'  => 'monobank_settings_form_ajax_wrap',
      'callback' => [$this, 'ajaxSubmit'],
    ];
    $this->config = \Drupal::config('monobank.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monobank_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form += [
      '#prefix' => '<div id="' . $this->ajax['wrapper'] . '">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages',
      ],
    ];

    $form['config'] = [
      '#tree' => TRUE,
      'currency' => [
        '#type' => 'select',
        '#options' => $this->monobank->getCurrency(),
        '#title' => $this->monobank->t('Currency'),
        '#default_value' => $this->config->get('config.currency'),
        '#required' => TRUE,
      ],
      'key' => [
        '#type' => 'textfield',
        '#title' => $this->monobank->t('X-Token'),
        '#default_value' => $this->config->get('config.key'),
      ],
      'ppo' => [
        '#type' => 'checkbox',
        '#title' => $this->monobank->t('Order PPO'),
        '#default_value' => $this->config->get('config.ppo'),
      ],
      'success' => [
        '#type' => 'text_format',
        '#title' => $this->monobank->t('Successful payment page text'),
        '#default_value' => $this->config->get('config.success.value'),
        '#format' => $this->config->get('config.success.format'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#name' => 'save',
        '#value' => t('Save configuration'),
        '#attributes' => [
          'class' => ['button--primary'],
        ],
        '#ajax' => $this->ajax,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('monobank.settings');
      $configSave = $form_state->getValue('config');
      $config->set('config', $configSave);
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
