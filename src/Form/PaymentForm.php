<?php

namespace Drupal\monobank\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SettingsForm.
 */
class PaymentForm extends FormBase {

  const API_URL = 'https://api.monobank.ua/api/merchant/invoice/';

  /**
   * Monobank service.
   *
   * @var \Drupal\monobank\Monobank
   */
  protected $monobank;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->monobank = \Drupal::service('Monobank');
    $this->config = \Drupal::config('monobank.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monobank_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = NULL) {
    // Payment alter:
    $this->basketPaymentFormAlter($form, $form_state, $payment);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {

    $payData = \Drupal::getContainer()->get('database')->select('payments_monobank', 'm')
      ->fields('m', ['data'])
      ->condition('m.id', $payment->id)
      ->execute()->fetchField();
    if (!empty($payData)) {
      $payData = unserialize($payData);
      if (!empty($payData['insert']['invoiceId'])) {
        return;
      }
    }

    $config = $this->config->get('config');
    $data = [
      'amount' => (int) ($payment->amount * 100),
      'ccy' => (int) $payment->currency,
      'redirectUrl' => Url::fromRoute('monobank.pages', [
        'page_type' => 'payment_result',
      ], [
        'query' => [
          'id' => $payment->id,
        ],
        'absolute' => TRUE,
      ])->toString(),
      'webHookUrl' => Url::fromRoute('monobank.pages', [
        'page_type' => 'status',
      ], [
        'query' => [
          'id' => $payment->id,
        ],
        'absolute' => TRUE,
      ])->toString(),
    ];

    if (!empty($config['ppo']) && \Drupal::hasService('Basket')) {
      $basket = \Drupal::getContainer()->get('Basket');

      $order = $basket->orders(NULL, $payment->nid);
      if ($order) {
        $order = $order->load();
        if ($order) {
          $info = [
            'reference' => $order->id,
          ];
          foreach ($order->items as $item) {
            $image = '';
            if (!empty($item->node_fields['img_uri'])) {
              $image = \Drupal::service('file_url_generator')->generateAbsoluteString($item->node_fields['img_uri']);
            }
            $info['basketOrder'][] = [
              'name' => $item->node_fields['title'],
              'qty' => (int) $item->count,
              'sum' => (int) (($item->price - ($item->price * (($item->discount['percent'] ?? 0) / 100))) * 100),
              'code' => $item->nid,
              'icon' => $image,
            ];
          }
          $data['merchantPaymInfo'] = $info;
        }
      }
    }

    // Alter:
    \Drupal::moduleHandler()->alter('monobank_payment_params', $data, $payment, $config);

    // Method give:
    $token = (string) @$config['key'];

    try {
      $client = \Drupal::httpClient();
      $response = $client->post($this::API_URL . 'create', [
        'body' => json_encode($data),
        'headers' => [
          'Content-Type' => 'application/json',
          'X-Token' => trim($token),
        ],
      ]);
      $resData = $response->getBody()->getContents();
      if (!empty($resData)) {
        $resData = @json_decode($resData, TRUE);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addMessage($e->getMessage(), 'error');
    }

    if (!empty($resData['invoiceId']) && !empty($resData['pageUrl'])) {
      $raw = unserialize($payment->data);
      $raw['insert'] = $resData;
      $update = (object) [
        'id' => $payment->id,
        'data' => serialize($raw),
      ];
      $this->monobank->update($update);
      $response = new RedirectResponse($resData['pageUrl']);
      $response->send();
      exit();
    }
    throw new NotFoundHttpException();
  }

}
