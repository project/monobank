<?php

namespace Drupal\monobank\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines the Monobank Payments form.
 */
class MonobankPaymentsForm extends FormBase {

  /**
   * Monobank object.
   *
   * @var object|\Drupal\monobank\Monobank|null
   */
  protected object $monobank;

  const STATUSES = [
    'new' => 'New',
    'expired' => 'The validity period has expired',
    'created' => 'Payment created successfully, payment pending',
    'processing' => 'Payment is being processed',
    'success' => 'Successful payment',
    'failure' => 'Unsuccessful payment',
    'reversed' => 'The payment has been returned',
  ];

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->monobank = \Drupal::getContainer()->get('Monobank');
  }

  /**
   * Returns the form ID for the Monobank Payments form.
   */
  public function getFormId() {
    return 'monobank_payments';
  }

  /**
   * Build the form for payments.
   *
   * @param array $form
   *   The form definition array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $getCurrency = $this->monobank->getCurrency();
    $form['#title'] = $this->monobank->t('Payments');
    $form['filter'] = [
      '#type' => 'details',
      '#title' => $this->monobank->t('Filter'),
    ];
    $form['filter']['status'] = [
      '#type' => 'select',
      '#title' => $this->monobank->t('Status'),
      '#options' => $this->getOptionsStatus(),
      '#empty_option' => t('All'),
      '#default_value' => @$_GET['status'],
    ];
    $form['filter']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->monobank->t('Search'),
    ];
    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => 'ID',
          'field' => 'l.id',
          'sort' => 'desc',
        ],
        [
          'data' => $this->monobank->t('Created'),
          'field' => 'l.created',
        ],
        [
          'data' => 'NID',
          'field' => 'l.nid',
        ],
        [
          'data' => 'SID',
          'field' => 'l.sid',
        ],
        [
          'data' => $this->monobank->t('User ID'),
          'field' => 'l.uid',
        ],
        [
          'data' => $this->monobank->t('Amount'),
          'field' => 'l.amount',
        ],
        [
          'data' => $this->monobank->t('Status'),
          'field' => 'l.status',
        ],
        [
          'data' => $this->monobank->t('Payment time'),
          'field' => 'l.paytime',
        ],
      ],
    ];
    foreach ($this->getPays($form['table']['#header']) as $row) {

      $row->data = !empty($row->data) ? unserialize($row->data) : [];

      $form['table'][$row->id]['id'] = [
        '#markup' => $row->id,
      ];
      $form['table'][$row->id]['created'] = [
        '#markup' => date('d.m.Y H:i', $row->created),
      ];
      $form['table'][$row->id]['nid'] = [
        '#markup' => !empty($row->nid) ? $row->nid : '- - -',
      ];
      $form['table'][$row->id]['sid'] = [
        '#markup' => !empty($row->sid) ? $row->sid : '- - -',
      ];
      $form['table'][$row->id]['user'] = [
        '#markup' => !empty($row->uid) ? $row->uid : '- - -',
      ];
      $form['table'][$row->id]['amount'] = [
        '#markup' => $row->amount . ' (' . ($getCurrency[$row->currency] ?? '- - -') . ')',
      ];
      $form['table'][$row->id]['status'] = [
        '#markup' => $this->getPayStatus($row),
      ];
      $form['table'][$row->id]['paytime'] = [
        '#markup' => !empty($row->paytime) ? date('d.m.Y H:i', $row->paytime) : '- - -',
      ];
      if ($row->status == 'new') {
        $form['table'][$row->id]['paytime']['#markup'] = Url::fromRoute('monobank.pages', [
          'page_type' => 'pay',
        ], [
          'absolute' => TRUE,
          'query' => ['pay_id' => $row->id],
        ])->toString();
      }
    }
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

  /**
   * Submit the form and redirect to the payments page with the selected status.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = $_GET;
    $query['status'] = $form_state->getValue('status');
    $url = Url::fromRoute('monobank.payments', [], ['query' => $query]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * Retrieve the payments with optional filtering and pagination.
   *
   * @param string $header
   *   The sort header.
   */
  public function getPays($header) {
    $query = \Drupal::database()->select('payments_monobank', 'l');
    $query->fields('l');
    if (!empty($_GET['status'])) {
      $query->condition('l.status', $_GET['status']);
    }
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(50);
    return $pager->execute()->fetchAll();
  }

  /**
   * Retrieve the payment status.
   *
   * @param object $row
   *   The payment row object.
   */
  public function getPayStatus($row) {
    $status = !empty($row->status) ? $row->status : '- - -';
    if (!empty(self::STATUSES[$status])) {
      $status = self::STATUSES[$status];
    }
    return $status;
  }

  /**
   * Retrieve the options for payment status.
   */
  public function getOptionsStatus() {
    $query = \Drupal::database()->select('payments_monobank', 'l');
    $query->fields('l', ['status', 'status']);
    $options = $query->execute()->fetchAllKeyed();
    foreach ($options as $key => $value) {
      if (!empty(self::STATUSES[$key])) {
        $options[$key] = self::STATUSES[$key];
      }
    }
    return $options;
  }

}
