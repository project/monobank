<?php

/**
 * @file
 * Hooks provided by the monobank module.
 */

/**
 * Change of parameters that go to the payment.
 *
 * @param array $params
 *   Params data.
 * @param object $payment
 *   Payment object.
 * @param array $config
 *   Config data.
 */
function hook_monobank_payment_params_alter(array &$params, $payment, array &$config) {

}

/**
 * Alter API.
 *
 * @param object $payment
 *   Payment object.
 * @param array $fields
 *   Payment fields.
 */
function hook_monobank_api_alter($payment, array $fields) {

}

/**
 * Alter Currency list.
 *
 * @param array $list
 *   Currency list.
 */
function hook_monobank_currency_list_alter(&$list) {

}