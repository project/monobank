This module provides a [Drupal AlternativeCommerce (Basket)](https://www.drupal.org/project/basket) payment method to embed the payment services provided by [Monobank API](https://api.monobank.ua/docs/acquiring.html).

## Dependencies
* [Drupal AlternativeCommerce (Basket)](https://www.drupal.org/project/basket)

## Setup

* After installation, go to the payment systems settings 
page <code>/admin/basket/settings-payment</code>.
* Create a payment point and indicate in the service "Monobank".
* After you create, you will have a button to go to the settings 
page of the payment gateway itself. Or you can follow 
the link <code>/admin/config/development/monobank</code>.
* The module can also work in test mode.
